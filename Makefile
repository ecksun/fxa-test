.PHONY: test
test: test-py test-js

.PHONY: test-js
test-js: node_modules
	npm start

node_modules:
	npm install

.PHONY: test-py
test-py: venv
	venv/bin/python client.py

venv:
	virtualenv venv
	venv/bin/pip install PyFxA

.PHONY: clean
clean:
	rm -rf venv
