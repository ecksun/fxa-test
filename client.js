'use strict';

const FxAccountsClient = require('fxa-js-client');

const client = new FxAccountsClient('https://api.accounts.firefox.com/v1');

function printResponse(prefix) {
    return (resp) => {
        console.log(`Response for ${prefix}:`);
        console.dir(resp);
        return resp;
    };
}

client.signIn(process.env.SYNC_EMAIL, process.env.SYNC_PASSWORD, {
    keys: true
}) .then(creds => {
    return client.recoveryEmailStatus(creds.sessionToken);
}).then(resp => {
    console.log(resp);
});
