from httplib import HTTPConnection
HTTPConnection.debuglevel = 1

import os
from fxa.core import Client

client = Client("https://api.accounts.firefox.com")

session = client.login(os.environ['SYNC_EMAIL'], os.environ['SYNC_PASSWORD'], keys=True)
email_status = session.get_email_status()
print 'status: ',
print email_status
